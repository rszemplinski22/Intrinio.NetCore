# C# Wrapper for Intrinio

[![Build status](https://ci.appveyor.com/api/projects/status/0n77hypruaeodhay/branch/master?svg=true)](https://ci.appveyor.com/project/RyanSzemplinski/intrinio-netcore/branch/master)

The structure of this wrapper was heavily influenced by the [PinSharp](https://github.com/Krusen/PinSharp)

This project is a open source api client library to interact with the [Intrinio Api](http://docs.intrinio.com/#introduction) built for dotnet core.

## Installation

Intrinio.NetCore is be available on [NuGet](https://www.nuget.org/packages/Intrinio.NetCore.Api/)

## Usage

Below is an example using the client to get 

```
var client = new IntrinioClient({api-key}, {api-password})
var companies = await client.PublicCompanyDataFeed.GetCompanies();
```

## Contributing

I welcome anyone to submit pull requests and issues for this project. It is a work in progress, so currently I have built the pieces I needed for my use case, but would love to make it more robust and useful for more people. I will try to respond to issues as quickly as I can get to them.

## What needs to be done

I guess the better question is what is already done because it would be easier to create that list.

Here is a list of what needs to completed:

* ~~U.S Public Company Data Feed~~
* ~~Usage~~
* ~~Master Data Feed~~
* ~~Wall Street Horizon Feed~~
* Insider Transactions & Ownership Data
* Institutional Holdings Data Feed
* Economic Data Feed
* U.S Sector & Industry Data Feed
* U.S Bank Data Feed
* IEX Realtime Price Data Feed
* EDI Security Prices Data Feed
* EDI Corporate Actions Data Feed
* Zacks Current Estimate Data Feed
* Zacks Historical Estimates Data Feed
* Zacks Sales Surprises
* Zacks EPS Surprises
* Trade Alert US Option Data Feed
* TipRanks News Sentiment Data Feed
* TipRanks Blogger Rating Data Feed
* TipRanks Analyst Rating Data Feed
* Nasdaq Press Releases Data Feed

## Developers
- Github - [rszemplinski](https://github.com/rszemplinski)
- GitLab - [rszemplinski22](https://gitlab.com/rszemplinski22)

## Open Source Credits:

### [Flurl](https://github.com/tmenier/Flurl)

<details>
  <summary>Click to expand License</summary>
The MIT License (MIT)

Copyright (c) 2014 Todd Menier

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
</details>


### [Newtonsoft.Json](https://github.com/JamesNK/Newtonsoft.Json)

<details>
  <summary>Click to expand License</summary>
The MIT License (MIT)

Copyright (c) 2007 James Newton-King

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
</details>

### Microsoft - Dotnet Core / ASPNet Core / Entity Framework Core / Various other libraries
<details>
  <summary>Click to expand License</summary>
The MIT License (MIT)
Copyright (c) Microsoft Corporation

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
</details>