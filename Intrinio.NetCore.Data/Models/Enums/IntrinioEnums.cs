﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace Intrinio.NetCore.Data.Models.Enums
{

    public enum Tag
    {
        [EnumMember(Value = "52_week_high")]
        _52WeekHigh,
        [EnumMember(Value = "52_week_low")]
        _52WeekLow,
        [EnumMember(Value = "accountspayable")]
        AccountsPayable,
        [EnumMember(Value = "apturnover")]
        AccountsPayableTurnover,
        [EnumMember(Value = "accountsreceivable")]
        AccountsReceivable,
        [EnumMember(Value = "arturnover")]
        AccountsReceivableTurnover,
        [EnumMember(Value = "accruedexpenses")]
        AccruedExpenses,
        [EnumMember(Value = "accruedinterestpayable")]
        AccruedInterestPayable,
        [EnumMember(Value = "accruedinvestmentincome")]
        AccruedInvestmentIncome,
        [EnumMember(Value = "accumulateddepreciation")]
        AccumulatedDepreciation,
        [EnumMember(Value = "aoci")]
        AccumulatedOtherComprehensiveIncomeLoss,
        [EnumMember(Value = "acquisitions")]
        Acquisitions,
        [EnumMember(Value = "adj_close_price")]
        AdjustedClosePrice,
        [EnumMember(Value = "adj_high_price")]
        AdjustedHighPrice,
        [EnumMember(Value = "adj_low_price")]
        AdjustedLowPrice,
        [EnumMember(Value = "adj_open_price")]
        AdjustedOpenPrice,
        [EnumMember(Value = "adj_volume")]
        AdjustedVolume,
        [EnumMember(Value = "allowanceforloanandleaselosses")]
        AllowanceForLoanAndLeaseLosses,
        [EnumMember(Value = "altmanzscore")]
        AltmanZScore,
        [EnumMember(Value = "amortizationexpense")]
        AmortizationExpense,
        [EnumMember(Value = "amortizationofdeferredpolicyacquisitioncosts")]
        AmortizationOfDeferredPolicyAcquisitionCosts,
        [EnumMember(Value = "ask_price")]
        AskPrice,
        [EnumMember(Value = "ask_size")]
        AskSize,
        [EnumMember(Value = "ask_timestamp")]
        AskTimestamp,
        [EnumMember(Value = "assetretirementandlitigationobligation")]
        AssetRetirementReserveAndLitigationObligation,
        [EnumMember(Value = "assetturnover")]
        AssetTurnover,
        [EnumMember(Value = "augmentedpayoutratio")]
        AugmentedPayoutRatio,
        [EnumMember(Value = "average_daily_volume")]
        AverageDailyVolume,
        [EnumMember(Value = "bankersacceptances")]
        BankersAcceptanceOutstanding,
        [EnumMember(Value = "basicdilutedeps")]
        BasicAndDilutedEPS,
        [EnumMember(Value = "basiceps")]
        BasicEPS,
        [EnumMember(Value = "beta")]
        Beta,
        [EnumMember(Value = "bid_price")]
        BidPrice,
        [EnumMember(Value = "bid_size")]
        BidSize,
        [EnumMember(Value = "bid_timestamp")]
        BidTimestamp,
        [EnumMember(Value = "bookvaluepershare")]
        BookValuePerShare,
        [EnumMember(Value = "business_address")]
        BusinessAddress,
        [EnumMember(Value = "business_phone_no")]
        BusinessPhoneNumber,
        [EnumMember(Value = "capex")]
        CapitalExpenditures,
        [EnumMember(Value = "capitalleaseobligations")]
        CapitalLeaseObligations,
        [EnumMember(Value = "capitalizedleaseobligationinterestexpense")]
        CapitalizedLeaseObligationsInterestExpense,
        [EnumMember(Value = "cashandequivalents")]
        CashAndEquivalents,
        [EnumMember(Value = "ccc")]
        CashConversionCycle,
        [EnumMember(Value = "cashdividendspershare")]
        CashDividendsToCommonPerShare,
        [EnumMember(Value = "cashincometaxespaid")]
        CashIncomeTaxesPaid,
        [EnumMember(Value = "cashinterestpaid")]
        CashInterestPaid,
        [EnumMember(Value = "cashinterestreceived")]
        CashInterestReceived,
        [EnumMember(Value = "croic")]
        CashReturnOnInvestedCapital,
        [EnumMember(Value = "change")]
        ChangeInPrice,
        [EnumMember(Value = "increasedecreaseinoperatingcapital")]
        ChangeInOperatingAssetsAndLiabilities,
        [EnumMember(Value = "ceo")]
        CEO,
        [EnumMember(Value = "claimsandclaimexpenses")]
        ClaimsAndClaimExpense,
        [EnumMember(Value = "close_price")]
        ClosePrice,
        [EnumMember(Value = "commitmentsandcontingencies")]
        CommitmentsAndContigencies,
        [EnumMember(Value = "commontocap")]
        CommonEquityToTotalCapital,
        [EnumMember(Value = "commonequity")]
        CommonStock,
        [EnumMember(Value = "legal_name")]
        CompanyLegalName,
        [EnumMember(Value = "name")]
        CompanyName,
        [EnumMember(Value = "company_url")]
        CompanyWebsite,
        [EnumMember(Value = "compoundleveragefactor")]
        CompoundLeverageFactor,
        [EnumMember(Value = "netincome")]
        ConsolidatedNetIncome,
        [EnumMember(Value = "costofrevtorevenue")]
        CostOfRevenueToRevenue,
        [EnumMember(Value = "country")]
        Country,
        [EnumMember(Value = "currentandfuturebenefits")]
        CurrentAndFutureBenefits,
        [EnumMember(Value = "currentdeferredtaxliabilities")]
        CurrentDeferredTaxLiabilities,
        [EnumMember(Value = "currentdeferredtaxassets")]
        CurrentDeferredRefundableIncomeTaxes,
        [EnumMember(Value = "currentdeferredrevenue")]
        CurrentDeferredRevenue,
        [EnumMember(Value = "currentemployeebenefitliabilities")]
        CurrentEmployeeBenefitLiabilities,
        [EnumMember(Value = "currentratio")]
        CurrentRatio,
        [EnumMember(Value = "currentyearearningsyield")]
        CurrentYearForecastedEarningsYield,
        [EnumMember(Value = "customerandotherreceivables")]
        CustomerAndOtherReceivables,
        [EnumMember(Value = "customerdeposits")]
        CustomerDeposits,
        [EnumMember(Value = "dio")]
        DaysInventoryOutstanding,
        [EnumMember(Value = "dpo")]
        DaysPayableOutstanding,
        [EnumMember(Value = "dso")]
        DaysSaleOutstanding,
        [EnumMember(Value = "days_to_cover")]
        DaysToCover,
        [EnumMember(Value = "debttoebitda")]
        DebtToEBITDA,
        [EnumMember(Value = "debttoequity")]
        DebtToEquity,
        [EnumMember(Value = "debttonopat")]
        DebtToNOPAT,
        [EnumMember(Value = "debttototalcapital")]
        DebtToTotalCapital,
        [EnumMember(Value = "dfnwc")]
        DebtFreeNetWorkingCapital,
        [EnumMember(Value = "dfnwctorev")]
        DebtFreeNetWorkingCapitalToRevenue,
        [EnumMember(Value = "dfcfnwc")]
        DebtFreeCashFreeNetWorkingCapital,
        [EnumMember(Value = "dfcfnwctorev")]
        DebtFreeCashFreeNetWorkingCapitalToRevenue,
        [EnumMember(Value = "deferredacquisitioncost")]
        DeferredAcquisitionCost,
        [EnumMember(Value = "depletionexpense")]
        DepletionExpense,
        [EnumMember(Value = "depositsinterestincome")]
        DepositsAndMoneyMarketInvestmentsInterestIncome,
        [EnumMember(Value = "depositsinterestexpense")]
        DepositsInterestExpense,
        [EnumMember(Value = "depreciationexpense")]
        DepreciationExpense,
        [EnumMember(Value = "dilutedeps")]
        DilutedEPS,
        [EnumMember(Value = "divestitures")]
        Divestitures,
        [EnumMember(Value = "dividend_ex_date")]
        DividendExDate,
        [EnumMember(Value = "dividend_pay_date")]
        DividendPayDate,
        [EnumMember(Value = "divpayoutratio")]
        DividendPayoutRatio,
        [EnumMember(Value = "dividend")]
        DividendPerShare,
        [EnumMember(Value = "dividendyield")]
        DividendYield,
        [EnumMember(Value = "dividendspayable")]
        DividendsPayable,
        [EnumMember(Value = "ebit")]
        EarningsBeforeInterestAndTaxes,
        [EnumMember(Value = "ebitda")]
        EBITDA,
        [EnumMember(Value = "earningsyield")]
        EarningsYield,
        [EnumMember(Value = "ebitgrowth")]
        EBITGrowth,
        [EnumMember(Value = "ebitlesscapextointerestex")]
        EBITLessCapExToInterestExpense,
        [EnumMember(Value = "ebitmargin")]
        EBITMargin,
        [EnumMember(Value = "ebitqoqgrowth")]
        EBITQQGrowth,
        [EnumMember(Value = "ebittointerestex")]
        EBITToInterestExpense,
        [EnumMember(Value = "ebitdagrowth")]
        EBITDAGrowth,
        [EnumMember(Value = "ebitdamargin")]
        EBITDAMargin,
        [EnumMember(Value = "ebitdaqoqgrowth")]
        EBITDAQQGrowth,
        [EnumMember(Value = "effectofexchangeratechanges")]
        EffectOfExchangeRateChanges,
        [EnumMember(Value = "efftaxrate")]
        EffectiveTaxRate,
        [EnumMember(Value = "employeebenefitassets")]
        EmployeeBenefitAssets,
        [EnumMember(Value = "employees")]
        Employees,
        [EnumMember(Value = "enterprisevalue")]
        EnterpriseValue,
        [EnumMember(Value = "evtocurrentyearrevenue")]
        EnterpriseValueToCurrentYearForecastRevenue,
        [EnumMember(Value = "evtonextyearrevenue")]
        EnterpriseValueToNextYearForecastRevenue,
        [EnumMember(Value = "evtoebit")]
        EnterpriseValueToEBIT,
        [EnumMember(Value = "evtoebitda")]
        EnterpriseValueToEBITDA,
        [EnumMember(Value = "evtofcff")]
        EnterpriseValueToFreeCashFlow,
        [EnumMember(Value = "evtoinvestedcapital")]
        EnterpriseValueToInvestedCapital,
        [EnumMember(Value = "evtonopat")]
        EnterpriseValueToNOPAT,
        [EnumMember(Value = "evtoocf")]
        EnterpriseValueToOperatingCashFlow,
        [EnumMember(Value = "evtorevenue")]
        EnterpriseValueToRevenue,
        [EnumMember(Value = "epsgrowth")]
        EPSGrowth,
        [EnumMember(Value = "epsqoqgrowth")]
        EPSQQGrowth,
        [EnumMember(Value = "ex_dividend")]
        ExDividend,
        [EnumMember(Value = "exchange_status")]
        ExchangeFinancialStatus,
        [EnumMember(Value = "explorationexpense")]
        ExplorationExpense,
        [EnumMember(Value = "extraordinaryincome")]
        ExtraordinaryIncomeLossNet,
        [EnumMember(Value = "fedfundspurchased")]
        FederalFundsPurchasedAndSecuritiesSold,
        [EnumMember(Value = "fedfundsandrepointerestexpense")]
        FederalFundsPurchasedAndSecuritiesSoldInterestExpense,
        [EnumMember(Value = "fedfundssold")]
        FederalFundsSold,
        [EnumMember(Value = "fedfundsandrepointerestincome")]
        FederalFundsSoldAndSecuritesBorrowedInterestIncome,
        [EnumMember(Value = "finleverage")]
        FinancialLeverage,
        [EnumMember(Value = "five_yr_ave_dividend_yield")]
        FiveYearAverageDividendRate,
        [EnumMember(Value = "five_yr_monthly_beta")]
        FiveYearMonthlyBeta,
        [EnumMember(Value = "five_yr_weekly_beta")]
        FiveYearWeeklyBeta,
        [EnumMember(Value = "faturnover")]
        FixedAssetTurnover,
        [EnumMember(Value = "forward_dividend_rate")]
        ForwardDividendRate,
        [EnumMember(Value = "forward_dividend_yield")]
        ForwardDividendYield,
        [EnumMember(Value = "fcffgrowth")]
        FreeCashFlowFirmGrowth,
        [EnumMember(Value = "fcffqoqgrowth")]
        FreeCashFlowFirmQQGrowth,
        [EnumMember(Value = "freecashflow")]
        FreeCashFlowToFirm,
        [EnumMember(Value = "fcfftointerestex")]
        FreeCashFlowToFirmToInterestExpense,
        [EnumMember(Value = "futurepolicybenefits")]
        FuturePolicyBenefits,
        [EnumMember(Value = "goodwill")]
        Goodwill,
        [EnumMember(Value = "grossmargin")]
        GrossMargin,
        [EnumMember(Value = "high_price")]
        HighPrice,
        [EnumMember(Value = "impairmentexpense")]
        ImpairmentCharge,
        [EnumMember(Value = "incometaxexpense")]
        IncomeTaxExpense,
        [EnumMember(Value = "inc_country")]
        IncorporatedCountry,
        [EnumMember(Value = "inc_state")]
        IncorporatedState,
        [EnumMember(Value = "investedcapitalincreasedecrease")]
        IncreaseDecreaseInInvestedCapital,
        [EnumMember(Value = "industry_category")]
        IndustryCategory,
        [EnumMember(Value = "industry_group")]
        IndustryGroup,
        [EnumMember(Value = "policyacquisitioncosts")]
        InsurancePolicyAcquisitionCosts,
        [EnumMember(Value = "intangibleassets")]
        IntangibleAssets,
        [EnumMember(Value = "totalinterestincome")]
        InterestInvestmentIncome,
        [EnumMember(Value = "interestbearingdeposits")]
        InterestBearingDeposits,
        [EnumMember(Value = "interestbearingdepositsatotherbanks")]
        InterestBearingDepositsAtOtherBanks,
        [EnumMember(Value = "interestburdenpct")]
        InterestBurdenPercent,
        [EnumMember(Value = "totalinterestexpense")]
        InterestExpense,
        [EnumMember(Value = "netinventory")]
        InventoriesNet,
        [EnumMember(Value = "invturnover")]
        InventoryTurnover,
        [EnumMember(Value = "investedcapital")]
        InvestedCapital,
        [EnumMember(Value = "investedcapitalgrowth")]
        InvestedCapitalGrowth,
        [EnumMember(Value = "investedcapitalqoqgrowth")]
        InvestedCapitalQQGrowth,
        [EnumMember(Value = "investedcapitalturnover")]
        InvestedCapitalTurnover,
        [EnumMember(Value = "investmentbankingincome")]
        InvestmentBankingIncome,
        [EnumMember(Value = "investmentsecuritiesinterestincome")]
        InvestmentSecuritiesInterestIncome,
        [EnumMember(Value = "issuanceofcommonequity")]
        IssuanceOfCommonEquity,
        [EnumMember(Value = "issuanceofdebt")]
        IssuanceOfDebt,
        [EnumMember(Value = "issuanceofpreferredequity")]
        IssuanceOfPreferredEquity,
        [EnumMember(Value = "last_price")]
        LastPrice,
        [EnumMember(Value = "last_size")]
        LastSize,
        [EnumMember(Value = "last_timestamp")]
        LastTimestamp,
        [EnumMember(Value = "leverageratio")]
        LeverageRatio,
        [EnumMember(Value = "listing_exchange")]
        ListingExchange,
        [EnumMember(Value = "loansandleases")]
        LoansAndLeases,
        [EnumMember(Value = "loansandleaseinterestincome")]
        LoansAndLeasesInterestIncome,
        [EnumMember(Value = "netloansandleases")]
        LoansAndLeasesNetOfAllowance,
        [EnumMember(Value = "loansheldforsale")]
        LoansHeldForSale,
        [EnumMember(Value = "loansheldforsalenet")]
        LoansHeldForSaleNet,
        [EnumMember(Value = "long_description")]
        LongDescription,
        [EnumMember(Value = "longtermdebt")]
        LongTermDebt,
        [EnumMember(Value = "longtermdebtinterestexpense")]
        LongTermDebtInterestExpense,
        [EnumMember(Value = "ltdebttoebitda")]
        LongTermDebtToEBITDA,
        [EnumMember(Value = "ltdebttoequity")]
        LongTermDebtToEquity,
        [EnumMember(Value = "ltdebttonopat")]
        LongTermDebtToNOPAT,
        [EnumMember(Value = "ltdebttocap")]
        LongTermDebtToTotalCapital,
        [EnumMember(Value = "longterminvestments")]
        LongTermInvestments,
        [EnumMember(Value = "low_price")]
        LowPrice,
        [EnumMember(Value = "mailing_address")]
        MailingAddress,
        [EnumMember(Value = "marketcap")]
        MarketCapitalization,
        [EnumMember(Value = "market_category")]
        MarketCategory,
        [EnumMember(Value = "marketingexpense")]
        MarketingExpense,
        [EnumMember(Value = "mortgageservicingrights")]
        MortgageServicingRights,
        [EnumMember(Value = "netcashfromcontinuingfinancingactivities")]
        NetCashFromContinuingFianancingActivities,
        [EnumMember(Value = "netcashfromcontinuinginvestingactivities")]
        NetCashFromContinuingInvestingActivities,
        [EnumMember(Value = "netcashfromcontinuingoperatingactivities")]
        NetCashFromContinuingOperatingActivities,
        [EnumMember(Value = "netcashfromdiscontinuedfinancingactivities")]
        NetCashFromDisContinuedFinancingActivities,
        [EnumMember(Value = "netcashfromdiscontinuedinvestingactivities")]
        NetCashFromDisContinuedInvestingActivities,
        [EnumMember(Value = "netcashfromdiscontinuedoperatingactivities")]
        NetCashFromDisContinuedOperatingActivities,
        [EnumMember(Value = "netcashfromfinancingactivities")]
        NetCashFromFinancingActivities,
        [EnumMember(Value = "netcashfrominvestingactivities")]
        NetCashFromInvestingActivities,
        [EnumMember(Value = "netcashfromoperatingactivities")]
        NetCashFromOperatingActivities,
        [EnumMember(Value = "netchangeincash")]
        NetChangeInCashEquivalents,
        [EnumMember(Value = "netchangeindeposits")]
        NetChangeInDeposits,
        [EnumMember(Value = "netdebt")]
        NetDebt,
        [EnumMember(Value = "netdebttoebitda")]
        NetDebtToEBITDA,
        [EnumMember(Value = "netdebttonopat")]
        NetDebtToNOPAT,
        [EnumMember(Value = "netincometocommon")]
        NetIncomeLossAttributableToCommonShareholders,
        [EnumMember(Value = "netincometononcontrollinginterest")]
        NetIncomeLossAttributableToNonControllingInterest,
        [EnumMember(Value = "netincomecontinuing")]
        NetIncomeLossContinuingOperations,
        [EnumMember(Value = "netincomediscontinued")]
        NetIncomeLossDiscontinuedOperations,
        [EnumMember(Value = "netincomegrowth")]
        NetIncomeGrowth,
        [EnumMember(Value = "netincomeqoqgrowth")]
        NetIncomeQQGrowth,
        [EnumMember(Value = "netincreaseinfedfundssold")]
        NetIncreaseInFedFundsSold,
        [EnumMember(Value = "netinterestincome")]
        NetInterestIncomeExpense,
        [EnumMember(Value = "netnonopex")]
        NetNonOperatingExpense,
        [EnumMember(Value = "nnep")]
        NetNonOperatingExpensePercent,
        [EnumMember(Value = "netnonopobligations")]
        NetNonOperatingObligations,
        [EnumMember(Value = "netoccupancyequipmentexpense")]
        NetOccupancyAndOperatingExpense,
        [EnumMember(Value = "nopat")]
        NetOperatingProfitAfterTax,
        [EnumMember(Value = "netrealizedcapitalgains")]
        NetRealizedAndUnrealizedCapitalGainsOnInvestments,
        [EnumMember(Value = "nwc")]
        NetWorkingCapital,
        [EnumMember(Value = "nwctorev")]
        NetWorkingCapitalToRevenue,
        [EnumMember(Value = "nextyearearningsyield")]
        NextYearForecastedEarningsYield,
        [EnumMember(Value = "noncashadjustmentstonetincome")]
        NonCashAdjustmentsToReconcileNetIncome,
        [EnumMember(Value = "noninterestbearingdeposits")]
        NonInterestBearingDeposits,
        [EnumMember(Value = "noncontrollinginterests")]
        NonControllingInterest,
        [EnumMember(Value = "noncontrollinginterestsharingratio")]
        NonControllingInterestSharingRatio,
        [EnumMember(Value = "noncontrolinttocap")]
        NonControllingInterestsToTotalCapital,
        [EnumMember(Value = "noncurrentdeferredtaxliabilities")]
        NonCurrentDeferredAndPayableIncomeTaxLiabilities,
        [EnumMember(Value = "noncurrentdeferredtaxassets")]
        NonCurrentDeferredAndRefundableIncomeTaxes,
        [EnumMember(Value = "noncurrentdeferredrevenue")]
        NonCurrentDeferredRevenue,
        [EnumMember(Value = "noncurrentemployeebenefitliabilities")]
        NonCurrentEmployeeBenefitLiabilities,
        [EnumMember(Value = "noncurrentnotereceivables")]
        NonCurrentNoteAndLeaseReceivables,
        [EnumMember(Value = "nonoperatingincome")]
        NonOperatingIncomeExpenseNet,
        [EnumMember(Value = "nopatgrowth")]
        NOPATGrowth,
        [EnumMember(Value = "nopatlesscapextointex")]
        NOPATLessCapExToInterestExpense,
        [EnumMember(Value = "nopatmargin")]
        NOPATMargin,
        [EnumMember(Value = "nopatqoqgrowth")]
        NOPATQQGrowth,
        [EnumMember(Value = "nopattointerestex")]
        NOPATToInterestExpense,
        [EnumMember(Value = "normalizednopat")]
        NormalizedNetOperatingProfitAfterTax,
        [EnumMember(Value = "normalizednopatmargin")]
        NormalizedNOPATMargin,
        [EnumMember(Value = "notereceivable")]
        NoteAndLeaseReceivable,
        [EnumMember(Value = "one_yr_monthly_beta")]
        OneYearMonthlyBeta,
        [EnumMember(Value = "one_yr_weekly_beta")]
        OneYearWeeklyBeta,
        [EnumMember(Value = "open_price")]
        OpenPrice,
        [EnumMember(Value = "ocfgrowth")]
        OperatingCashFlowGrowth,
        [EnumMember(Value = "ocflesscapextointerestex")]
        OperatingCashFlowLessCapExToInterestExpense,
        [EnumMember(Value = "ocfqoqgrowth")]
        OperatingCashFlowQQGrowth,
        [EnumMember(Value = "ocftocapex")]
        OperatingCashFlowToCapEx,
        [EnumMember(Value = "ocftointerestex")]
        OperatingCashFlowToInterestExpense,
        [EnumMember(Value = "operatingcostofrevenue")]
        OperatingCostOfRevenue,
        [EnumMember(Value = "opextorevenue")]
        OperatingExpensesToRevenue,
        [EnumMember(Value = "operatingmargin")]
        OperatingMargin,
        [EnumMember(Value = "oroa")]
        OperatingReturnOnAssets,
        [EnumMember(Value = "operatingrevenue")]
        OperatingRevenue,
        [EnumMember(Value = "otheradjustmentstoconsolidatednetincome")]
        OtherAdjustmentsToConsolidatingNetIncomeLoss,
        [EnumMember(Value = "otheradjustmentstonetincometocommon")]
        OtherAdjustmentsToNetIncomeLossAttributableToCommonShareholders,
        [EnumMember(Value = "otherassets")]
        OtherAssets,
        [EnumMember(Value = "othercostofrevenue")]
        OtherCostOfRevenue,
        [EnumMember(Value = "othercurrentassets")]
        OtherCurrentAssets,
        [EnumMember(Value = "othercurrentliabilities")]
        OtherCurrentLiabilities,
        [EnumMember(Value = "othercurrentnonoperatingassets")]
        OtherCurrentNonOperatingAssets,
        [EnumMember(Value = "othercurrentnonoperatingliabilities")]
        OtherCurrentNonOperatingLiabilities,
        [EnumMember(Value = "otherequity")]
        OtherEquityAdjustments,
        [EnumMember(Value = "otherfinancingactivitiesnet")]
        OtherFinancingActivitiesNet,
        [EnumMember(Value = "othergains")]
        OtherGainsLossesNet,
        [EnumMember(Value = "otherincome")]
        OtherIncomeExpenseNet,
        [EnumMember(Value = "otherinterestexpense")]
        OtherInterestExpense,
        [EnumMember(Value = "otherinterestincome")]
        OtherInterestIncome,
        [EnumMember(Value = "otherinvestingactivitiesnet")]
        OtherInvestingActivitiesNet,
        [EnumMember(Value = "otherlongtermliabilities")]
        OtherLongTermLiabilities,
        [EnumMember(Value = "othernetchangesincash")]
        OtherNetChangesInCash,
        [EnumMember(Value = "othernoninterestincome")]
        OtherNonInterestIncome,
        [EnumMember(Value = "othernoncurrentnonoperatingassets")]
        OtherNonCurrentNonOperatingAssets,
        [EnumMember(Value = "othernoncurrentnonoperatingliabilities")]
        OtherNonCurrentNonOperatingLiabilities,
        [EnumMember(Value = "othernoncurrentassets")]
        OtherNonCurrentOperatingAssets,
        [EnumMember(Value = "othernoncurrentliabilities")]
        OtherNonCurrentOperatingLiabilities,
        [EnumMember(Value = "otheroperatingexpenses")]
        OtherOperatingExpensesIncome,
        [EnumMember(Value = "otherrevenue")]
        OtherRevenue,
        [EnumMember(Value = "otherservicechargeincome")]
        OtherServiceCharges,
        [EnumMember(Value = "othershorttermpayables")]
        OtherShortTermPayables,
        [EnumMember(Value = "otherspecialcharges")]
        OtherSpecialChargesIncome,
        [EnumMember(Value = "othertaxespayable")]
        OtherTaxesPayable,
        [EnumMember(Value = "participatingpolicyholderequity")]
        ParticipatingPolicyHolderEquity,
        [EnumMember(Value = "paymentofdividends")]
        PaymentsOfDividends,
        [EnumMember(Value = "percent_change")]
        PercentChangeInPrice,
        [EnumMember(Value = "grossppe")]
        PlantPropertyEquipmentGross,
        [EnumMember(Value = "netppe")]
        PlantPropertyEquipmentNet,
        [EnumMember(Value = "policyholderfunds")]
        PolicyHolderFunds,
        [EnumMember(Value = "pretaxincomemargin")]
        PreTaxIncomeMargin,
        [EnumMember(Value = "preferredtocap")]
        PreferredEquityToTotalCapital,
        [EnumMember(Value = "totalpreferredequity")]
        PreferredStock,
        [EnumMember(Value = "preferreddividends")]
        PreferredStockDividendsDeclared,
        [EnumMember(Value = "netpremisesandequipment")]
        PremisesAndEquipmentNet,
        [EnumMember(Value = "premiumsearned")]
        PremiumsEarned,
        [EnumMember(Value = "prepaidexpenses")]
        PrepaidExpenses,
        [EnumMember(Value = "price_date")]
        PriceDate,
        [EnumMember(Value = "price_time")]
        PriceTime,
        [EnumMember(Value = "pricetobook")]
        PriceToBookValue,
        [EnumMember(Value = "pricetocurrentyearearnings")]
        PriceToCurrentYearForecastedEarnings,
        [EnumMember(Value = "pricetocurrentyearrevenue")]
        PriceToCurrentYearForecastedRevenue,
        [EnumMember(Value = "pricetoearnings")]
        PriceToEarnings,
        [EnumMember(Value = "pricetonextyearearnings")]
        PriceToNextYearForecastedEarnings,
        [EnumMember(Value = "pricetonextyearrevenue")]
        PriceToNextYearForecastedRevenue,
        [EnumMember(Value = "pricetorevenue")]
        PriceToRevenue,
        [EnumMember(Value = "pricetotangiblebook")]
        PriceToTangibleBookValue,
        [EnumMember(Value = "profitmargin")]
        ProfitMargin,
        [EnumMember(Value = "propertyliabilityinsuranceclaims")]
        PropertyAndLiabilityInsuranceClaims,
        [EnumMember(Value = "provisionforcreditlosses")]
        ProvisionForCreditLosses,
        [EnumMember(Value = "provisionforloanlosses")]
        ProvisionForLoanLosses,
        [EnumMember(Value = "purchaseofinvestments")]
        PurchaseOfInvestments,
        [EnumMember(Value = "purchaseofplantpropertyandequipment")]
        PurchaseOfPropertyPlantAndEquipment,
        [EnumMember(Value = "quickratio")]
        QuickRatio,
        [EnumMember(Value = "rdextorevenue")]
        RDToRevenue,
        [EnumMember(Value = "redeemablenoncontrollinginterest")]
        RedeemableNonControllingInterest,
        [EnumMember(Value = "repaymentofdebt")]
        RepaymentOfDebt,
        [EnumMember(Value = "repurchaseofcommonequity")]
        RepurchaseOfCommonEquity,
        [EnumMember(Value = "repurchaseofpreferredequity")]
        RepurchaseOfPreferredEquity,
        [EnumMember(Value = "rdexpense")]
        ResearchAndDevelopmentExpense,
        [EnumMember(Value = "restrictedcash")]
        RestrictedCash,
        [EnumMember(Value = "restructuringcharge")]
        RestructuringCharge,
        [EnumMember(Value = "retainedearnings")]
        RetainedEarnings,
        [EnumMember(Value = "roa")]
        ReturnOnAssets,
        [EnumMember(Value = "roce")]
        ReturnOnCommonEquity,
        [EnumMember(Value = "roe")]
        ReturnOnEquity,
        [EnumMember(Value = "roic")]
        ReturnOnInvestedCapital,
        [EnumMember(Value = "rnnoa")]
        ReturnOnNetNonOperatingAssets,
        [EnumMember(Value = "revenuegrowth")]
        RevenueGrowth,
        [EnumMember(Value = "revenueqoqgrowth")]
        RevenueQQGrowth,
        [EnumMember(Value = "roicnnepspread")]
        ROICLessNNEPSpread,
        [EnumMember(Value = "salariesandemployeebenefitsexpense")]
        SalariesAndEmployeeBenefits,
        [EnumMember(Value = "saleofinvestments")]
        SaleAndOrMaturityOfInvestments,
        [EnumMember(Value = "saleofplantpropertyandequipment")]
        SaleOfPropertyPlantAndEquipment,
        [EnumMember(Value = "cik")]
        SECCentralIndexKey,
        [EnumMember(Value = "sector")]
        Sector,
        [EnumMember(Value = "security_class")]
        SecurityClass,
        [EnumMember(Value = "security_name")]
        SecurityName,
        [EnumMember(Value = "security_round_lot_size")]
        SecurityRoundLotSize,
        [EnumMember(Value = "security_ticker")]
        SecurityTickerSymbol,
        [EnumMember(Value = "security_type")]
        SecurityType,
        [EnumMember(Value = "sgaexpense")]
        SellingGeneralAdminExpense,
        [EnumMember(Value = "separateaccountbusinessassets")]
        SeperateAccountBusinessAssets,
        [EnumMember(Value = "separateaccountbusinessliabilities")]
        SeperateAccountBusinessLiabilites,
        [EnumMember(Value = "servicechargesondepositsincome")]
        ServiceChargesOnDepositAccounts,
        [EnumMember(Value = "seven_yr_monthly_beta")]
        SevenYearMonthlyBeta,
        [EnumMember(Value = "seven_yr_weekly_beta")]
        SevenYearWeeklyBeta,
        [EnumMember(Value = "sgaextorevenue")]
        SGAExpensesToRevenue,
        [EnumMember(Value = "short_description")]
        ShortDescription,
        [EnumMember(Value = "short_interest")]
        ShortInterest,
        [EnumMember(Value = "shorttermborrowinginterestexpense")]
        ShortTermBorrowingInterestExpense,
        [EnumMember(Value = "shorttermdebt")]
        ShortTermDebt,
        [EnumMember(Value = "stdebttocap")]
        ShortTermDebtToTotalCapital,
        [EnumMember(Value = "shortterminvestments")]
        ShortTermInvestments,
        [EnumMember(Value = "split_ratio")]
        SplitRatio,
        [EnumMember(Value = "sic")]
        StandardizedIndustrialClassificationCode,
        [EnumMember(Value = "state")]
        State,
        [EnumMember(Value = "stock_exchange")]
        StockExchange,
        [EnumMember(Value = "tangbookvaluepershare")]
        TangibleBookValuePerShare,
        [EnumMember(Value = "taxburdenpct")]
        TaxBurdenPercent,
        [EnumMember(Value = "template")]
        Template,
        [EnumMember(Value = "ten_yr_monthly_beta")]
        TenYearMonthlyBeta,
        [EnumMember(Value = "ten_yr_weekly_beta")]
        TenYearWeeklyBeta,
        [EnumMember(Value = "three_yr_monthly_beta")]
        ThreeYearMonthlyBeta,
        [EnumMember(Value = "three_yr_weekly_beta")]
        ThreeYearWeeklyBeta,
        [EnumMember(Value = "ticker")]
        TickerSymbol,
        [EnumMember(Value = "timedepositsplaced")]
        TimeDepositsPlacedAndOtherShortTermInvestments,
        [EnumMember(Value = "totalassets")]
        TotalAssets,
        [EnumMember(Value = "totalcapital")]
        TotalCapital,
        [EnumMember(Value = "totalcommonequity")]
        TotalCommonEquity,
        [EnumMember(Value = "totalcostofrevenue")]
        TotalCostOfRevenue,
        [EnumMember(Value = "totalcurrentassets")]
        TotalCurrentAssets,
        [EnumMember(Value = "totalcurrentliabilities")]
        TotalCurrentLiabilities,
        [EnumMember(Value = "debt")]
        TotalDebt,
        [EnumMember(Value = "depreciationandamortization")]
        TotalDepreciationAndAmortization,
        [EnumMember(Value = "totalequityandnoncontrollinginterests")]
        TotalEquityAndNonControllingInterests,
        [EnumMember(Value = "totalgrossprofit")]
        TotalGrossProfit,
        [EnumMember(Value = "totalliabilities")]
        TotalLiabilities,
        [EnumMember(Value = "totalliabilitiesandequity")]
        TotalLiabilitiesAndShareholdersEquity,
        [EnumMember(Value = "ltdebtandcapleases")]
        TotalLongTermDebt,
        [EnumMember(Value = "totalnoninterestexpense")]
        TotalNonInterestExpense,
        [EnumMember(Value = "totalnoninterestincome")]
        TotalNonInterestIncome,
        [EnumMember(Value = "totalnoncurrentassets")]
        TotalNonCurrentAssets,
        [EnumMember(Value = "totalnoncurrentliabilities")]
        TotalNonCurrentLiabilities,
        [EnumMember(Value = "totaloperatingexpenses")]
        TotalOperatingExpenses,
        [EnumMember(Value = "totaloperatingincome")]
        TotalOperatingIncome,
        [EnumMember(Value = "totalotherincome")]
        TotalOtherIncomeExpenseNet,
        [EnumMember(Value = "totalpretaxincome")]
        TotalPreTaxIncome,
        [EnumMember(Value = "totalequity")]
        TotalPreferredAndCommonEquity,
        [EnumMember(Value = "totalrevenue")]
        TotalRevenue,
        [EnumMember(Value = "tradingaccountinterestincome")]
        TradingAccountInterestIncome,
        [EnumMember(Value = "tradingaccountsecurities")]
        TradingAccountSecurities,
        [EnumMember(Value = "dividend_rate")]
        TrailingDividendRate,
        [EnumMember(Value = "trailing_dividend_yield")]
        TrailingDividendYield,
        [EnumMember(Value = "treasurystock")]
        TreasuryStock,
        [EnumMember(Value = "trustfeeincome")]
        TrustFeesByCommission,
        [EnumMember(Value = "unearnedpremiumsdebit")]
        UnearnedPremiumAssets,
        [EnumMember(Value = "unearnedpremiumscredit")]
        UnearnedPremiumLiabilities,
        [EnumMember(Value = "volume")]
        Volume,
        [EnumMember(Value = "weightedavebasicdilutedsharesos")]
        WeightedAverageBasicAndDilutedSharesOutstanding,
        [EnumMember(Value = "weightedavebasicsharesos")]
        WeightedAverageBasicSharesOutstanding,
        [EnumMember(Value = "weightedavedilutedsharesos")]
        WeightedAverageDilutedSharesOutstanding
    }

    public enum ReportType
    {
        [EnumMember(Value = "10-K")]
        _10K,
        [EnumMember(Value = "10-Q")]
        _10Q,
        [EnumMember(Value = "8-K")]
        _8K,
        [EnumMember(Value = "4")]
        _4,
        [EnumMember(Value = "etc")]
        Etc
    }

    public enum StockIndex
    {
        [EnumMember(Value = "$SPX")]
        SP500,
        [EnumMember(Value = "$DJI")]
        DowJones,
        [EnumMember(Value = "$NYA")]
        NyseComposite,
        [EnumMember(Value = "$NDX")]
        SP100,
        [EnumMember(Value = "$RUT")]
        Russell2000,
        [EnumMember(Value = "$STOXX50")]
        STOXX50,
        [EnumMember(Value = "$DAX")]
        GermanDAX,
        [EnumMember(Value = "$CAC")]
        FrenchCAC40,
        [EnumMember(Value = "$HSI")]
        HongKongHengSeng,
        [EnumMember(Value = "$SSEC")]
        ShanghaiComposite,
        [EnumMember(Value = "$NIKK")]
        TokyoNikkei225,
        [EnumMember(Value = "$AORD")]
        ASXAllOrdinaries,
        [EnumMember(Value = "$TA100")]
        TelAvivTA100
    }

    public enum StockExchangeEnum
    {
        [EnumMember(Value = "^XNAS")]
        Nasdaq,
        [EnumMember(Value = "^XNYS")]
        Nyse,
        [EnumMember(Value = "^BATS")]
        BatsZExchange,
        [EnumMember(Value = "^XBRU")]
        EuronextBrussels,
        [EnumMember(Value = "^XTSE")]
        TorontoStockExchange,
        [EnumMember(Value = "^XSHG")]
        ShanghaiStockExchange,
        [EnumMember(Value = "^XSHE")]
        ShenzenStockExchange,
        [EnumMember(Value = "^XPAR")]
        EuronextParis,
        [EnumMember(Value = "^XFRA")]
        DeutscheBoerse,
        [EnumMember(Value = "^XJPX")]
        JapanExchangeGroup,
        [EnumMember(Value = "^XLIS")]
        EuronextLisbon,
        [EnumMember(Value = "^XSWX")]
        SIXSwissExchange,
        [EnumMember(Value = "^XAMS")]
        EuronextAmsterdam,
        [EnumMember(Value = "^XLON")]
        LondonStockExchange,
        [EnumMember(Value = "^XHKG")]
        HKExchangeAndClearing,
        [EnumMember(Value = "^XBOM")]
        BSE,
        [EnumMember(Value = "^XNSE")]
        NationalStockExchangeOfIndia,
        [EnumMember(Value = "^BMEX")]
        BME,
        [EnumMember(Value = "^XSTO")]
        NasdaqStockholm,
        [EnumMember(Value = "^XTAI")]
        TaiwanStockExchange,
        [EnumMember(Value = "^BVMF")]
        BMF,
        [EnumMember(Value = "^XTSX")]
        TSXVentureExchange,
        [EnumMember(Value = "^XMEX")]
        BolsaMexicanaDeValores,
        [EnumMember(Value = "^XTAE")]
        TelAvivStockExchange,
        [EnumMember(Value = "^XMIL")]
        BorsaItaliana,
        [EnumMember(Value = "^XKRX")]
        KoreaStockExchange,
        [EnumMember(Value = "^XSAU")]
        SaudiStockExchange,
        [EnumMember(Value = "^MISX")]
        MoscowExchange,
        [EnumMember(Value = "^XSES")]
        SingaporeExchange,
        [EnumMember(Value = "^XOTC")]
        OTCBB
    }

    public enum WallStreetHorizonTag
    {
        [EnumMember(Value = "next_earnings_date")]        
        NextEarningsDate,
        [EnumMember(Value = "next_earnings_quarter")]
        NextEarningsQuarter,
        [EnumMember(Value = "next_earnings_year")]
        NextEarningsYear,
        [EnumMember(Value = "next_earnings_time_of_day")]
        NextEarningsTimeOfDay,
        [EnumMember(Value = "q1_earnings_date")]
        Q1EarningsDate,
        [EnumMember(Value = "q2_earnings_date")]
        Q2EarningsDate,
        [EnumMember(Value = "q3_earnings_date")]
        Q3EarningsDate,
        [EnumMember(Value = "q4_earnings_date")]
        Q4EarningsDate,
        [EnumMember(Value = "conference_call_date")]
        ConferenceCallDate,
        [EnumMember(Value = "conference_call_time")]
        ConferenceCallTime,
        [EnumMember(Value = "conference_call_phone_number")]
        ConferenceCallPhoneNumber,
        [EnumMember(Value = "conference_call_passcode")]
        ConferenceCallPasscode,
        [EnumMember(Value = "conference_call_url")]
        ConferenceCallUrl,
        [EnumMember(Value = "transcript_quarter")]
        TranscriptQuarter,
        [EnumMember(Value = "transcript_year")]
        TranscriptYear,
        [EnumMember(Value = "transcript_url")]
        TranscriptUrl,
        [EnumMember(Value = "dividend_frequency")]
        DividendFrequency,
        [EnumMember(Value = "last_ex_dividend")]
        LastExDividend,
        [EnumMember(Value = "last_ex_div_currency")]
        LastExDividendCurrency,
        [EnumMember(Value = "last_adj_ex_dividend")]
        LastAdjustedDividend,
        [EnumMember(Value = "last_div_announce_date")]
        LastDividendAnnounceDate,
        [EnumMember(Value = "last_div_record_date")]
        LastDividendRecordDate,
        [EnumMember(Value = "last_div_pay_date")]
        LastDividendPayDate,
        [EnumMember(Value = "last_ex_div_date")]
        LastExDivdendDate,
        [EnumMember(Value = "dividend_status")]
        DividendStatus
    }


    public enum Order
    {
        Popularity,
        Symbol
    }

    public enum IndexType
    {
        [EnumMember(Value = "stock_market")]
        StockMarket,
        [EnumMember(Value = "economic")]
        Economic,
        [EnumMember(Value = "sic")]
        SIC
    }

    public enum FiscalPeriod
    {
        FY,
        Q1,
        Q2,
        Q3,
        Q4,
        Q1TTM,
        Q2TTM,
        Q3TTM,
        Q2YTD,
        Q3YTD
    }

    public enum Statement
    {
        [EnumMember(Value = "income_statement")]
        IncomeStatement,
        [EnumMember(Value = "balance_sheet")]
        BalanceSheet,
        [EnumMember(Value = "cash_flow_statement")]
        CashFlowStatement,
        [EnumMember(Value = "calculations")]
        Calculations
    }

    public enum AccessCode
    {
        [EnumMember(Value = "com_sec_master_data")]
        ComSECMasterData,
        [EnumMember(Value = "com_fin_data")]
        ComFinData,
        [EnumMember(Value = "industry_data")]
        IndustryData,
        [EnumMember(Value = "fdic_data")]
        FDICData,
        [EnumMember(Value = "econ_data")]
        EconData,
        [EnumMember(Value = "insider_trans")]
        InsiderTrans,
        [EnumMember(Value = "inst_hold")]
        InstHold
    }

    public enum Template
    {
        Industrial,
        Financial
    }

    public enum Frequency
    {
        Daily,
        Weekly,
        Monthly,
        Quarterly,
        Yearly,
    }

    public enum PeriodType
    {
        [Description("Fiscal Year")]
        FY,
        [Description("Quarterly")]
        QTR,
        [Description("Trailing Twelve Months")]
        TTM,
        [Description("Year To Date")]
        YTD
    }

    public enum SortOrder
    {
        [EnumMember(Value = "asc")]
        Ascending,
        [EnumMember(Value = "desc")]
        Descending
    }

    public enum Operator
    {
        [EnumMember(Value = "eq")]
        EqualTo,
        [EnumMember(Value = "lt")]
        LessThan,
        [EnumMember(Value = "lte")]
        LessThanOrEqualTo,
        [EnumMember(Value = "gt")]
        GreaterThan,
        [EnumMember(Value = "gte")]
        GreaterThanOrEqualTo,
        [EnumMember(Value = "contains")]
        Contains
    }

    public enum LogicalOperator
    {
        And,
        Or
    }
}
