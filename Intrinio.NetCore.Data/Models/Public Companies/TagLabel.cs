using Intrinio.NetCore.Data.Models;
using Intrinio.NetCore.Data.Extensions;
using Newtonsoft.Json;
using Intrinio.NetCore.Data.Models.Enums;

namespace Intrinio.NetCore.Data.Models.Public_Companies
{
    public class TagLabel
    {
        [JsonProperty("name")]
        public string Name;

        [JsonProperty("tag")]
        public string Tag;

        [JsonIgnore]
        public Tag? TagEnum
        {
            get
            {
                if (string.IsNullOrEmpty(Tag))
                    return null;

                return Tag.GetEnumFromMember<Tag>();
            }
        }

        [JsonProperty("parent")]
        public string Parent;

        [JsonProperty("factor")]
        public string Factor;

        [JsonProperty("balance")]
        public string Balance;

        [JsonProperty("type")]
        public string Type;

        [JsonProperty("units")]
        public string Units;
    }
}