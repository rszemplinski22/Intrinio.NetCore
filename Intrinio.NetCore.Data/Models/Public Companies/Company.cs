using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Intrinio.NetCore.Data.Models.Public_Companies
{
    public class Company
    {
        [JsonProperty("ticker")]
        public string Ticker;

        [JsonProperty("cik")]
        public string CentralIndexKey;

        [JsonProperty("name")]
        public string Name;

        [JsonProperty("legal_name")]
        public string LegalName;

        [JsonProperty("lei")]
        public string LegalEntityIdentifier;

        [JsonProperty("hq_address1")]
        public string HQAddress1;

        [JsonProperty("hq_address2")]
        public string HQAddress2;
        
        [JsonProperty("hq_address_city")]
        public string HQAddressCity;

        [JsonProperty("hq_address_postal_code")]
        public string HQAddressPostalCode;

        [JsonProperty("entity_legal_form")]
        public string EntityLegalForm;

        [JsonProperty("hq_state")]
        public string HQState;

        [JsonProperty("hq_country")]
        public string HQCountry;

        [JsonProperty("inc_state")]
        public string IncState;

        [JsonProperty("inc_country")]
        public string IncCountry;

        [JsonProperty("sic")]
        public int? StandardIndustrialClassification;

        [JsonProperty("securities")]
        public List<Security> Securities;

        [JsonProperty("stock_exchange")]
        public string StockExchange;

        [JsonProperty("template")]
        public string Template;

        [JsonProperty("short_description")]
        public string ShortDescription;

        [JsonProperty("long_description")]
        public string LongDescription;

        [JsonProperty("ceo")]
        public string CEO;

        [JsonProperty("company_url")]
        public string CompanyUrl;

        [JsonProperty("business_address")]
        public string BusinessAddress;

        [JsonProperty("mailing_address")]
        public string MailingAddress;

        [JsonProperty("business_phone_no")]
        public string BusinessPhoneNumber;

        [JsonProperty("employees")]
        public int? Employees;

        [JsonProperty("sector")]
        public string Sector;

        [JsonProperty("industry_category")]
        public string IndustryCategory;

        [JsonProperty("industry_group")]
        public string IndustryGroup;

        [JsonProperty("standardized_active")]
        public bool StandardizedActive;

        [JsonProperty("latest_filing_date")]
        public DateTime? LatestFilingDate;
    }
}