using Newtonsoft.Json;

namespace Intrinio.NetCore.Data.Models.Public_Companies
{
    public class ExchangePrice : Price
    {
        [JsonProperty("ticker")]
        public string Ticker;

        [JsonProperty("figi_ticker")]
        public string FigiTicker;

        [JsonProperty("figi")]
        public string Figi;
    }
}