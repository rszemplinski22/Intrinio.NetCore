﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Intrinio.NetCore.Data.Models.Public_Companies
{
    public class XBRLTagLabel
    {

        [JsonProperty("name")]
        public string Name;

        [JsonProperty("tag")]
        public string Tag;

        [JsonProperty("balance")]
        public string Balance;

        [JsonProperty("unit")]
        public string Unit;

        [JsonProperty("domain_tag")]
        public string DomainTag;

        [JsonProperty("abstract")]
        public bool Abstract;

        [JsonProperty("sequence")]
        public int Sequence;

        [JsonProperty("depth")]
        public int Depth;

        [JsonProperty("factor")]
        public string Factor;

    }
}
