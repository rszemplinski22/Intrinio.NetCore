using System.Collections.Generic;
using System.Linq;
using Intrinio.NetCore.Data.Models.Enums;

namespace Intrinio.NetCore.Data.Models.Public_Companies.MetaData
{
    public class Condition
    {

        public List<Condition> Conditions { get; set; } = new List<Condition>();

        public List<Expression> Expressions { get; set; } = new List<Expression>();

        public LogicalOperator Operator { get; set; } = LogicalOperator.And;

        public bool IsNegated { get; set; } = false;

        public string BuildLogicString()
        {
            return BuildLogicStringHelper(0);
        }

        private string BuildLogicStringHelper(int currentIndex)
        {
            var logicString = "";

            if (IsNegated)
            {
                logicString += "NOT (";
            }

            logicString += BuildExpressionString(ref currentIndex);
            logicString = Conditions.Aggregate(logicString,
                (current, next) =>
                    $"{current} {Operator.ToString().ToUpper()} ({next.BuildLogicStringHelper(currentIndex)})");

            if (IsNegated)
            {
                logicString += ")";
            }

            return logicString;
        }

        private string BuildExpressionString(ref int currentIndex)
        {
            var expressionString = "";
            foreach (var expression in Expressions)
            {
                expression.Label = string.IsNullOrEmpty(expression.Label)
                    ? $"{Expression.DefaultLabelName}{currentIndex}"
                    : expression.Label;

                expressionString += $"{expression.Label}";

                if (expression != Expressions.Last())
                {
                    expressionString += $" {Operator.ToString().ToUpper()} ";
                }

                currentIndex++;
            }
            return expressionString;
        }

    }
}