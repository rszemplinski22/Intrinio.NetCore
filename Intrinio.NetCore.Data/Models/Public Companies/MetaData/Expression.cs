using System;
using Intrinio.NetCore.Data.Extensions;
using Intrinio.NetCore.Data.Models.Enums;

namespace Intrinio.NetCore.Data.Models.Public_Companies.MetaData
{
    public class Expression
    {

        public const string DefaultLabelName = "label";

        public Tag DataTag { get; set; }

        ///<summary>
        /// Use this only if the Tag enum does not contain the value you need
        ///</summary>
        public string DataTagString { get; set; }

        public Operator Operator { get; set; }

        public IComparable Value { get; set; }

        public string Label { get; set; }

        public override string ToString()
        {
            var dataTag = string.IsNullOrEmpty(DataTagString) ? DataTag.GetAlternateValue() : DataTagString;

            var expressionString = $"{dataTag} {Operator.GetAlternateValue()} {Value}";
            if (!string.IsNullOrEmpty(Label))
            {
                expressionString += $" {Label}";
            }

            return expressionString;
        }
    }
}