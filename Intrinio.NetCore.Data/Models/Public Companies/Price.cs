using System;
using Newtonsoft.Json;

namespace Intrinio.NetCore.Data.Models.Public_Companies
{
    public class Price
    {
        [JsonProperty("date")]
        public DateTime Date;

        [JsonProperty("open")]
        public decimal Open;
        
        [JsonProperty("high")]        
        public decimal High;

        [JsonProperty("low")]
        public decimal Low;

        [JsonProperty("close")]
        public decimal Close;

        [JsonProperty("volume")]
        public decimal Volume;

        [JsonProperty("ex_dividend")]
        public decimal ExDividend;

        [JsonProperty("split_ratio")]
        public decimal SplitRatio;

        [JsonProperty("adj_open")]
        public decimal AdjOpen;

        [JsonProperty("adj_high")]
        public decimal AdjHigh;

        [JsonProperty("adj_low")]
        public decimal AdjLow;
        
        [JsonProperty("adj_close")]
        public decimal AdjClose;

        [JsonProperty("adj_volume")]
        public decimal AdjVolume;
    }
}