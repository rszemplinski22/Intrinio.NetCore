using System;
using Intrinio.NetCore.Data.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Intrinio.NetCore.Data.Models.Enums;

namespace Intrinio.NetCore.Data.Models.Public_Companies
{
    public class Fundamental
    {
        [JsonProperty("fiscal_year")]
        public int FiscalYear;

        [JsonProperty("end_date")]
        public DateTime EndDate;

        [JsonProperty("start_date")]
        public DateTime StartDate;
        
        [JsonProperty("filing_date")]
        public DateTime FilingDate;

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty("fiscal_period")]
        public FiscalPeriod FiscalPeriod; 
    }
}