﻿using Intrinio.NetCore.Data.Models.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace Intrinio.NetCore.Data.Models.Public_Companies
{
    public class SECFiling
    {

        [JsonProperty("filing_date")]
        public DateTime FilingDate;

        [JsonProperty("accepted_date")]
        public DateTime AcceptedDate;

        [JsonProperty("period_ended")]
        public DateTime PeriodEnded;

        [JsonProperty("account_number")]
        public string AccountNumber;

        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty("report_type")]
        public ReportType ReportType;

        [JsonProperty("filing_url")]
        public string FilingUrl;

        [JsonProperty("report_url")]
        public string ReportUrl;

        [JsonProperty("instance_url")]
        public string InstanceUrl;

    }
}
