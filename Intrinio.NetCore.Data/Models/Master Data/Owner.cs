﻿using Newtonsoft.Json;

namespace Intrinio.NetCore.Data.Models.Master_Data
{
    public class Owner
    {

        [JsonProperty("owner_cik")]
        public string OwnerCIK;

        [JsonProperty("owner_name")]
        public string OwnerName;

    }
}
