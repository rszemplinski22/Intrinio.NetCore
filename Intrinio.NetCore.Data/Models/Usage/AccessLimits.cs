﻿using Intrinio.NetCore.Data.Models.Enums;
using Newtonsoft.Json;
using System;

namespace Intrinio.NetCore.Data.Models.Usage
{

    public class AccessLimits
    {

        public AccessCode AccessCodeEnum;


        private string _accessCode;

        [JsonProperty("access_code")]
        public string AccessCode
        {
            get { return _accessCode; }
            set
            {
                _accessCode = value;
                var success = Enum.TryParse(value, out AccessCodeEnum);
            }
        }

        [JsonProperty("daily_limit")]
        public int DailyLimit;

    }
}
