﻿using System;
using System.Collections.Generic;
using System.Text;
using Intrinio.NetCore.Data.Models.Public_Companies.MetaData;

namespace Intrinio.NetCore.Data.Extensions
{
    public static class ExpressionExtensions
    {

        public static string ConvertToIntrinioString(this Expression expression)
        {
            return expression.ToString().Replace(" ", "~");
        }

    }
}
