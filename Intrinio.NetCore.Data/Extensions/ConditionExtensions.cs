using Intrinio.NetCore.Data.Models.Public_Companies.MetaData;
using System.Collections.Generic;
using System.Linq;
using Intrinio.NetCore.Data.Models.Enums;

namespace Intrinio.NetCore.Data.Extensions
{
    public static class ConditionExtensions
    {

        public static (string condition, string logic) ConvertToIntrinioParams(this Condition condition)
        {
            string conditionString;
            var expressions = condition.AggregateExpressions().ToList();

            // Don't bother with the logic string generation if it's all ANDs and no Negations
            // because Intrinio defaults to all ANDs anyways
            if (condition.AggregateConditions().All(x => x.Operator == LogicalOperator.And && !x.IsNegated))
            {
                conditionString = expressions.Select(x => x.ConvertToIntrinioString())
                    .Aggregate((current, next) => $"{current},{next}");
                return (conditionString, null);
            }

            var index = 0;
            foreach (var expression in expressions)
            {
                if (!string.IsNullOrEmpty(expression.Label)) continue;

                expression.Label = $"{Expression.DefaultLabelName}{index}";
                index++;
            }

            conditionString = expressions.Select(x => x.ConvertToIntrinioString())
                .Aggregate((current, next) => $"{current},{next}");

            var logicString = condition.BuildLogicString();
            return (conditionString, logicString);
        }

        public static IEnumerable<Expression> AggregateExpressions(this Condition condition)
        {
            var expressions = new List<Expression>();

            expressions.AddRange(condition.Expressions);

            foreach (var subCondition in condition.Conditions)
            {
                expressions.AddRange(subCondition.AggregateExpressions());
            }

            return expressions;
        }

        public static IEnumerable<Condition> AggregateConditions(this Condition condition)
        {
            var conditions = new List<Condition> {condition};

            foreach (var subCondition in condition.Conditions)
            {
                conditions.AddRange(subCondition.AggregateConditions());
            }

            return conditions;
        }
        
    }
}