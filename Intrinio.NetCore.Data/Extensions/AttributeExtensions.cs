using System;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace Intrinio.NetCore.Data.Extensions
{
    public static class EnumExtensions
    {
        
        public static string GetAlternateValue(this Enum val)
        {
            var type = val.GetType();
            var memInfo = type.GetTypeInfo().GetMember(val.ToString());
            var attributes = (EnumMemberAttribute []) memInfo[0].GetCustomAttributes(typeof(EnumMemberAttribute), false);
            return (attributes.Length > 0) ? attributes[0].Value : null;
        }

        public static T? GetEnumFromMember<T>(this string val) where T : struct
        {
            var type = typeof(T);
            if(!type.GetTypeInfo().IsEnum)
            {
                throw new ArgumentException();
            }

            var fields = type.GetTypeInfo().GetFields();
            var field = fields.SelectMany(x => x.GetCustomAttributes(typeof(EnumMemberAttribute)), 
                (f, a) => new { Field = f, Attr = a })
                .Where(a => ((EnumMemberAttribute) a.Attr).Value == val).SingleOrDefault();

            return field == null ? (T?) null : (T) field.Field.GetRawConstantValue();
        }

    }
}