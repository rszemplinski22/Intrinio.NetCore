using Newtonsoft.Json;

namespace Intrinio.NetCore.Responses
{
    public class ErrorResponse
    {
        [JsonProperty("human")]
        public string Human;

        [JsonProperty("message")]
        public string Message;
    }
}