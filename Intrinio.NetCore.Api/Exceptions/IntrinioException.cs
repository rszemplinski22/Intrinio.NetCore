using System;

namespace Intrinio.NetCore.Exceptions
{
    public class IntrinioException : Exception
    {
        public int? StatusCode { get; internal set; }

        public string RequestUrl { get; internal set; }

        public string ResponseContent { get; internal set; }

        public IntrinioException()
        {
        }

        public IntrinioException(string message) : base(message)
        {
        }

        public IntrinioException(string message, Exception inner) : base(message, inner)
        {
        }

        internal static T Create<T>(string message, string requestUrl, string responseContent, int? statusCode = null)
            where T : IntrinioException
        {
            var exception = (T) Activator.CreateInstance(typeof(T), message);
            exception.RequestUrl = requestUrl;
            exception.ResponseContent = responseContent;
            exception.StatusCode = statusCode;
            return exception;
        }
    }
}