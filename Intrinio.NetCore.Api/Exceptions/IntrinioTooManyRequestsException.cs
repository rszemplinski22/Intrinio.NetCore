namespace Intrinio.NetCore.Exceptions
{
    public class IntrinioTooManyRequestsException : IntrinioException
    {
        
        public IntrinioTooManyRequestsException(string message) : base(message)
        {
            StatusCode = 429;
        }

    }
}