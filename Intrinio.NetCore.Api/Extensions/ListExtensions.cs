using System.Collections.Generic;
using System.Linq;

namespace Intrinio.NetCore.Extensions
{
    public static class ListExtensions
    {
        
        public static string ToCsv<T>(this IEnumerable<T> list)
        {
            var csvString = "";
            var convertedList = list.ToList();

            for(var i = 0; i < convertedList.Count; i++)
            {

                csvString += convertedList[i];

                if(i != convertedList.Count - 1)
                {
                    csvString += ",";
                }
            }

            return csvString;
        }

    }
}