﻿using System;
using System.Collections.Generic;

namespace Intrinio.NetCore.Api.Extensions
{
    public static class DictionaryExtensions
    {

        public static void AddIfHasValue<T>(this Dictionary<T, object> dict, T key, object value)
        {
            switch (value)
            {
                case null:
                    return;

                case DateTime _:
                    dict.Add(key, ((DateTime) value).ToString("yyyy-MM-dd"));
                    return;
            }

            dict.Add(key, value);
        }

    }
}
