﻿using Intrinio.NetCore.Api.Api.Interfaces;
using System;
using Intrinio.NetCore.Data.Models.Enums;
using Intrinio.NetCore.Data.Models.Master_Data;
using Intrinio.NetCore.Data.Models.Public_Companies;
using Intrinio.NetCore.Responses;
using System.Threading.Tasks;
using System.Collections.Generic;
using Intrinio.NetCore.Api.Extensions;
using Intrinio.NetCore.Data.Extensions;

namespace Intrinio.NetCore.Api.Api
{
    internal partial class IntrinioApi : IMasterDataFeed
    {
        private const string OwnersEndpoint = "owners";
        private const string StockExchangesEndpoint = "stock_exchanges";

        public Task<MultiResult<Company>> GetCompanyMasterAsync(string query = null, DateTime? latestFilingDate = null, int pageSize = 250, int pageNumber = 1)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.AddIfHasValue("query", query);
            queryParams.AddIfHasValue("latest_filing_date", latestFilingDate);
            queryParams.Add("page_size", pageSize);
            queryParams.Add("page_number", pageNumber);

            return GetMultiResultAsync<Company>(CompanyEndpoint, queryParams);
        }

        public Task<MultiResult<Index>> GetIndexMasterAsync(string identifier, IndexType? type = null, Order? order = null, int pageSize = 250, int pageNumber = 1)
        {
            return GetIndexMasterInternal(identifier, null, type, order, pageSize, pageNumber);
        }

        public Task<MultiResult<Index>> GetIndexMasterByQueryAsync(string query, IndexType? type = null, Order? order = null, int pageSize = 250, int pageNumber = 1)
        {
            return GetIndexMasterInternal(null, query, type, order, pageSize, pageNumber);
        }

        private Task<MultiResult<Index>> GetIndexMasterInternal(string identifier = null, string query = null, IndexType? type = null, Order? order = null, int pageSize = 250, int pageNumber = 1)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.AddIfHasValue("identifier", identifier);
            queryParams.AddIfHasValue("query", query);
            queryParams.AddIfHasValue("type", type.GetAlternateValue());
            queryParams.AddIfHasValue("order", order.ToString().ToLower());
            queryParams.Add("page_size", pageSize);
            queryParams.Add("page_number", pageNumber);

            return GetMultiResultAsync<Index>(IndicesEndpoint, queryParams);
        }

        public Task<MultiResult<Owner>> GetOwnerMasterAsync(int pageSize = 250, int pageNumber = 1)
        {
            return GetOwnerMasterInternal(null, null, null, pageSize, pageNumber);
        }

        public Task<MultiResult<Owner>> GetOwnerMasterByInstitutionalAsync(bool institutional, int pageSize = 250, int pageNumber = 1)
        {
            return GetOwnerMasterInternal(institutional, null, null, pageSize, pageNumber);
        }

        public Task<MultiResult<Owner>> GetOwnerMasterByIndexAsync(string indexKey, int pageSize = 250, int pageNumber = 1)
        {
            return GetOwnerMasterInternal(null, indexKey, null, pageSize, pageNumber);
        }

        public Task<MultiResult<Owner>> GetOwnerMasterByQueryAsync(string query, int pageSize = 250, int pageNumber = 1)
        {
            return GetOwnerMasterInternal(null, null, query, pageSize, pageNumber);
        }

        public Task<MultiResult<Owner>> GetOwnerMasterInternal(bool? institutional = null, string indexKey = null, string query = null, int pageSize = 250, int pageNumber = 1)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.AddIfHasValue("institutional", institutional);
            queryParams.AddIfHasValue("identifier", indexKey);
            queryParams.AddIfHasValue("query", query);
            queryParams.Add("page_size", pageSize);
            queryParams.Add("page_number", pageNumber);

            return GetMultiResultAsync<Owner>(OwnersEndpoint, queryParams);
        }

        public Task<MultiResult<Security>> GetSecurityMasterAsync(string identifier = null, string query = null, string exchange = null, bool usOnly = default(bool), int pageSize = 250, int pageNumber = 1)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.AddIfHasValue("identifier", identifier);
            queryParams.AddIfHasValue("query", query);
            queryParams.AddIfHasValue("exch_symbol", exchange);
            queryParams.Add("us_only", usOnly);
            queryParams.Add("page_size", pageSize);
            queryParams.Add("page_number", pageNumber);

            return GetMultiResultAsync<Security>(SecuritiesEndpoint, queryParams);
        }

        public Task<MultiResult<Security>> GetSecurityMasterAsync(string identifier = null, string query = null, StockExchangeEnum? exchange = null, bool usOnly = false, int pageSize = 250, int pageNumber = 1)
        {
            return GetSecurityMasterAsync(identifier, query, exchange.GetAlternateValue(), usOnly, pageSize, pageNumber);
        }

        public Task<MultiResult<StockExchange>> GetStockExchangeMasterAsync(string query = null, int pageSize = 250, int pageNumber = 1)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.AddIfHasValue("query", query);
            queryParams.Add("page_size", pageSize);
            queryParams.Add("page_number", pageNumber);

            return GetMultiResultAsync<StockExchange>(StockExchangesEndpoint, queryParams);
        }
    }
}
