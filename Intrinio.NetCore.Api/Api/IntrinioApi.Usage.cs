using System.Threading.Tasks;
using Intrinio.NetCore.Api.Interfaces;
using System.Collections.Generic;
using Intrinio.NetCore.Data.Models.Usage;
using Intrinio.NetCore.Data.Models.Enums;
using Intrinio.NetCore.Responses;
using Intrinio.NetCore.Data.Extensions;

namespace Intrinio.NetCore.Api.Api
{
    internal partial class IntrinioApi : IUsage
    {

        private const string UsageEndpoint = "usage";

        public Task<List<AccessLimits>> GetAccessLimitsAsync()
        {
            return GetAsync<List<AccessLimits>>($"{UsageEndpoint}/access");
        }

        public Task<CurrentUsage> GetCurrentUsageAsync(string accessCode)
        {
            return GetAsync<CurrentUsage>($"{UsageEndpoint}/current", new { access_code = accessCode });
        }

        public Task<CurrentUsage> GetCurrentUsageAsync(AccessCode accessCode)
        {
            return GetCurrentUsageAsync(accessCode.GetAlternateValue());
        }

        public Task<MultiResult<HistoricalUsage>> GetHistoricalUsageAsync(string accessCode)
        {
            return GetMultiResultAsync<HistoricalUsage>($"{UsageEndpoint}/historical", new { access_code = accessCode });
        }

        public Task<MultiResult<HistoricalUsage>> GetHistoricalUsageAsync(AccessCode accessCode)
        {
            return GetHistoricalUsageAsync(accessCode.ToString());
        }
    }
}