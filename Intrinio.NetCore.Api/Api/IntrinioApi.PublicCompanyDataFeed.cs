using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using Intrinio.NetCore.Api.Api.Interfaces;
using Intrinio.NetCore.Api.Extensions;
using Intrinio.NetCore.Extensions;
using Intrinio.NetCore.Data.Models.Public_Companies;
using Intrinio.NetCore.Data.Models.Enums;
using Intrinio.NetCore.Data.Extensions;
using Intrinio.NetCore.Exceptions;
using Intrinio.NetCore.Responses;
using Intrinio.NetCore.Data.Models.Public_Companies.MetaData;

namespace Intrinio.NetCore.Api.Api
{
    internal partial class IntrinioApi : IPublicCompanyDataFeed
    {

        private const string CompanyEndpoint = "companies";
        private const string SecuritiesEndpoint = "securities";
        private const string IndicesEndpoint = "indices";
        private const string DataPointEndpoint = "data_point";
        private const string HistoricalEndpoint = "historical_data";
        private const string PricesEndpoint = "prices";
        private const string FinancialsEndpoint = "financials";
        private const string FundamentalsEndpoint = "fundamentals";
        private const string NewsEndpoint = "news";
        private const string TagsEndpoint = "tags";

        public Task<MultiResult<SECFiling>> GetSECFilingsAsync(string symbol, ReportType? type = ReportType._4, DateTime? startDate = null, DateTime? endDate = null, int? pageSize = 250, int? pageNumber = 1)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.Add("identifier", symbol);
            queryParams.Add("report_type", type.GetAlternateValue());
            queryParams.AddIfHasValue("start_date", startDate);
            queryParams.AddIfHasValue("end_date", endDate);
            queryParams.Add("page_size", pageSize);
            queryParams.Add("page_number", pageNumber);

            return GetMultiResultAsync<SECFiling>($"{CompanyEndpoint}/filings", queryParams);
        }

        public Task<MultiResult<Company>> GetCompaniesAsync(DateTime? latestFilingDate = null, int pageSize = 250, int pageNumber = 1)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.Add("page_size", pageSize);
            queryParams.Add("page_number", pageNumber);
            queryParams.AddIfHasValue("latest_filing_date", latestFilingDate);

            return GetMultiResultAsync<Company>(CompanyEndpoint, queryParams);
        }

        public Task<Company> GetCompanyAsync(string symbol)
        {
            return GetAsync<Company>(CompanyEndpoint, new { identifier = symbol });
        }

        public Task<MultiResult<CompanyNews>> GetCompanyNewsAsync(string identifier, int pageSize = 250, int pageNumber = 1)
        {
            return GetMultiResultAsync<CompanyNews>(NewsEndpoint, 
                new { identifier = identifier, page_size = pageSize, page_number = pageNumber });
        }

        public Task<DataPoint> GetDataPointAsync(string identifier, string item)
        {
            return GetAsync<DataPoint>(DataPointEndpoint, new { identifier = identifier, item = item });
        }

        public Task<DataPoint> GetDataPointAsync(string identifier, Tag item)
        {
            return GetDataPointAsync(identifier, item.GetAlternateValue());
        }

        public Task<MultiResult<DataPoint>> GetDataPointFromIdentifiersAsync(string item, params string[] identifiers)
        {
            return GetDataPointsAsync(identifiers.AsEnumerable(), new List<string> { item });
        }

        public Task<MultiResult<DataPoint>> GetDataPointFromIdentifiersAsync(Tag item, params string[] identifiers)
        {
            return GetDataPointsAsync(identifiers.AsEnumerable(), new List<string> { item.GetAlternateValue() });
        }

        public Task<MultiResult<DataPoint>> GetDataPointsAsync(IEnumerable<string> identifiers, IEnumerable<string> items)
        {
            var identifierList = identifiers as IList<string> ?? identifiers.ToList();
            var itemList = items as IList<string> ?? items.ToList();
            
            if (identifierList.Count() * itemList.Count() > 150)
            {
                throw new IntrinioException("Identifier and item count exceeds 150. You may only request up to 150 identifiers/item combinations.");
            }

            return GetMultiResultAsync<DataPoint>(DataPointEndpoint, new { identifier = identifierList.ToCsv(), item = itemList.ToCsv() });
        }

        public Task<MultiResult<DataPoint>> GetDataPointsFromItemsAsync(string identifier, params string[] items)
        {
            return GetDataPointsAsync(new List<string> { identifier }, items.AsEnumerable());
        }

        public Task<MultiResult<DataPoint>> GetDataPointsFromItemsAsync(string identifier, params Tag[] items)
        {
            return GetDataPointsAsync(new List<string> { identifier }, items.Select(x => x.GetAlternateValue()));
        }

        public Task<MultiResult<HistoricalData>> GetHistoricalDataAsync(string identifier, string item, DateTime? startDate = null, DateTime? endDate = null, 
            Frequency frequency = Frequency.Yearly, PeriodType type = PeriodType.TTM, SortOrder sortOrder = SortOrder.Descending, int? pageSize = null, int? pageNumber = null)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.Add("identifier", identifier);
            queryParams.Add("item", item);
            queryParams.AddIfHasValue("start_date", startDate);
            queryParams.AddIfHasValue("end_date", endDate);
            queryParams.AddIfHasValue("frequency", frequency.ToString().ToLower());
            queryParams.AddIfHasValue("type", type);
            queryParams.AddIfHasValue("sort_order", sortOrder.GetAlternateValue());
            queryParams.AddIfHasValue("page_size", pageSize);
            queryParams.AddIfHasValue("page_number", pageNumber);

            return GetMultiResultAsync<HistoricalData>(HistoricalEndpoint, queryParams);
        }

        public Task<MultiResult<HistoricalData>> GetHistoricalDataAsync(string identifier, Tag item, DateTime? startDate = null, DateTime? endDate = null,
            Frequency frequency = Frequency.Yearly, PeriodType type = PeriodType.TTM, SortOrder sortOrder = SortOrder.Descending, int? pageSize = null, int? pageNumber = null)
        {
            return GetHistoricalDataAsync(identifier, item.GetAlternateValue(), startDate, endDate, frequency, type, sortOrder, pageSize, pageNumber);
        }

        public Task<MultiResult<Price>> GetPricesAsync(string identifier, DateTime? startDate = null, DateTime? endDate = null, Frequency frequency = Frequency.Yearly, 
            SortOrder sortOrder = SortOrder.Descending, int? pageSize = null, int? pageNumber = null)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.Add("identifier", identifier);
            queryParams.AddIfHasValue("start_date", startDate);
            queryParams.AddIfHasValue("end_date", endDate);
            queryParams.AddIfHasValue("frequency", frequency.ToString().ToLower());
            queryParams.AddIfHasValue("sort_order", sortOrder.GetAlternateValue());
            queryParams.AddIfHasValue("page_size", pageSize);
            queryParams.AddIfHasValue("page_number", pageNumber);

            return GetMultiResultAsync<Price>(PricesEndpoint, queryParams);
        }

        public Task<MultiResult<Financial>> GetStandardizedFinancialsAsync(string identifier, Statement statement, DateTime fiscalYear, 
            FiscalPeriod period, PeriodType? type = null, DateTime? date = null, int? pageSize = null, int? pageNumber = null)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.Add("identifier", identifier);
            queryParams.Add("statement", statement.GetAlternateValue());
            queryParams.Add("fiscal_year", fiscalYear.ToString("yyyy"));
            queryParams.Add("fiscal_period", period);
            queryParams.AddIfHasValue("type", type);
            queryParams.AddIfHasValue("date", date);
            queryParams.AddIfHasValue("page_size", pageSize);
            queryParams.AddIfHasValue("page_number", pageNumber);

            return GetMultiResultAsync<Financial>($"{FinancialsEndpoint}/standardized", queryParams);
        }

        public Task<MultiResult<ExchangePrice>> GetExchangePricesAsync(string exchange, DateTime priceDate, int? pageSize = null, int? pageNumber = null)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.Add("identifier", exchange);
            queryParams.Add("price_date", priceDate.ToString("yyyy-MM-dd"));
            queryParams.AddIfHasValue("page_size", pageSize);
            queryParams.AddIfHasValue("page_number", pageNumber);

            return GetMultiResultAsync<ExchangePrice>($"{PricesEndpoint}/exchange", queryParams);
        }

        public Task<MultiResult<ExchangePrice>> GetExchangePricesAsync(StockExchangeEnum exchange, DateTime priceDate, int? pageSize = null, int? pageNumber = null)
        {
            return GetExchangePricesAsync(exchange.GetAlternateValue(), priceDate, pageSize, pageNumber);
        }

        public Task<MultiResult<dynamic>> ScreenSecuritiesAsync(Condition condition, SortOrder sortOrder = SortOrder.Ascending, bool primaryOnly = false, int pageSize = 250, int pageNumber = 1)
        {
            if (condition.AggregateExpressions().Count() > 10)
            {
                throw new IntrinioException("Exceeded max number of expressions that can be sent in one request. (Max: 10)");
            }

            (string conditionString, string logicString) = condition.ConvertToIntrinioParams();
            return ScreenSecuritiesAsync(conditionString, logicString, sortOrder, primaryOnly, pageSize, pageNumber);
        }

        public Task<MultiResult<dynamic>> ScreenSecuritiesAsync(string condition, string logic = null, SortOrder sortOrder = SortOrder.Ascending, bool primaryOnly = false, int pageSize = 250, int pageNumber = 1)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.Add("conditions", CleanConditions(condition));
            queryParams.AddIfHasValue("logic", logic);
            queryParams.Add("order_direction", sortOrder.GetAlternateValue());
            queryParams.Add("primary_only", primaryOnly);
            queryParams.AddIfHasValue("page_size", pageSize);
            queryParams.AddIfHasValue("page_number", pageNumber);
            
            return GetMultiResultAsync<dynamic>($"{SecuritiesEndpoint}/search", queryParams);
        }

        private string CleanConditions(string conditions)
        {
            var splitConditions = conditions.Split(',');
            for(var i = 0; i < splitConditions.Length; i++)
            {
                splitConditions[i] = splitConditions[i].Trim().Replace(" ", "~").Replace(">", "gt")
                    .Replace(">=", "gte").Replace("<", "lt").Replace("<=", "lte");
            }
            return string.Join(",", splitConditions);
        }

        public Task<MultiResult<Security>> GetSecuritiesWithQueryAsync(string query, DateTime? lastCrspAdjDate = null, int pageSize = 250, int pageNumber = 1)
        {
            return GetSecuritiesInternal(query, null, lastCrspAdjDate, pageSize, pageNumber);
        }

        public Task<MultiResult<Security>> GetSecuritiesWithExchangeAsync(StockExchangeEnum? exchangeSymbol, DateTime? lastCrspAdjDate = null, int pageSize = 250, int pageNumber = 1)
        {
            return GetSecuritiesInternal(null, exchangeSymbol.GetAlternateValue(), lastCrspAdjDate, pageSize, pageNumber);
        }

        public Task<MultiResult<Security>> GetSecuritiesWithExchangeAsync(string exchangeSymbol, DateTime? lastCrspAdjDate = null, int pageSize = 250, int pageNumber = 1)
        {
            return GetSecuritiesInternal(null, exchangeSymbol, lastCrspAdjDate, pageSize, pageNumber);
        }

        public Task<MultiResult<Security>> GetAllSecuritiesAsync(int pageSize = 250, int pageNumber = 1)
        {
            return GetSecuritiesInternal(pageSize: pageSize, pageNumber: pageNumber);
        }

        private Task<MultiResult<Security>> GetSecuritiesInternal(string query = null, string exchangeSymbol = null, DateTime? lastCrspAdjDate = null, int pageSize = 250, int pageNumber = 1)
        {
            var queryParams = new Dictionary<string, object>();

            if(!string.IsNullOrEmpty(query) && !string.IsNullOrEmpty(exchangeSymbol))
            {
                throw new IntrinioException("Query and exchange symbol cannot both be populated for this request.");
            }

            queryParams.AddIfHasValue("query", query);
            queryParams.AddIfHasValue("exch_symbol", exchangeSymbol);
            queryParams.AddIfHasValue("last_crsp_adj_date", lastCrspAdjDate);
            queryParams.Add("page_size", pageSize);
            queryParams.Add("page_number", pageNumber);

            return GetMultiResultAsync<Security>(SecuritiesEndpoint, queryParams);
        }

        public Task<Security> GetSecurityAsync(string identifier)
        {
            return GetAsync<Security>(SecuritiesEndpoint, new { identifier = identifier });
        }

        public Task<MultiResult<Fundamental>> GetStandardizedFundamentalsAsync(string identifier, Statement statement, PeriodType type = PeriodType.FY, DateTime? date = default(DateTime?), int pageSize = 250, int pageNumber = 1)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.Add("identifier", identifier);
            queryParams.Add("statement", statement.GetAlternateValue());
            queryParams.Add("type", type);
            queryParams.AddIfHasValue("date", date);
            queryParams.Add("page_size", pageSize);
            queryParams.Add("page_number", pageNumber);

            return GetMultiResultAsync<Fundamental>($"{FundamentalsEndpoint}/standardized", queryParams);
        }

        public Task<MultiResult<TagLabel>> GetStandardizedTagsAndLabelsAsync(Statement statement, string ticker = null, Template? template = null, int? pageSize = null, int? pageNumber = null)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.Add("statement", statement.GetAlternateValue());
            queryParams.AddIfHasValue("identifier", ticker);
            queryParams.AddIfHasValue("template", template.ToString().ToLower());
            queryParams.AddIfHasValue("page_size", pageSize);
            queryParams.AddIfHasValue("page_number", pageNumber);

            return GetMultiResultAsync<TagLabel>($"{TagsEndpoint}/standardized", queryParams);
        }

        public Task<MultiResult<Index>> GetAllIndicesAsync(string type = null, int pageSize = 250, int pageNumber = 1)
        {
            return GetIndicesInternal(null, null, type, pageSize, pageNumber);
        }

        public Task<MultiResult<Index>> GetIndicesByQueryAsync(string query, string type = null, int pageSize = 250, int pageNumber = 1)
        {
            return GetIndicesInternal(null, query, type, pageSize, pageNumber);
        }

        private Task<MultiResult<Index>> GetIndicesInternal(string identifier = null, string query = null, string type = null, int pageSize = 250, int pageNumber = 1)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.AddIfHasValue("identifier", identifier);
            queryParams.AddIfHasValue("query", query);
            queryParams.AddIfHasValue("type", type);
            queryParams.Add("page_size", pageSize);
            queryParams.Add("page_number", pageNumber);

            return GetMultiResultAsync<Index>(IndicesEndpoint, queryParams);
        }

        public Task<Index> GetIndexAsync(StockIndex index)
        {
            return GetIndexAsync(index.GetAlternateValue());
        }

        public Task<Index> GetIndexAsync(string identifier)
        {
            return GetAsync<Index>(IndicesEndpoint, new { identifier = identifier });
        }

        public Task<MultiResult<Fundamental>> GetAsReportedFundamentalsAsync(string symbol, Statement statement, PeriodType? type = null, DateTime? date = null, int pageSize = 250, int pageNumber = 1)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.Add("identifier", symbol);
            queryParams.Add("statement", statement.GetAlternateValue());
            queryParams.AddIfHasValue("type", type);
            queryParams.AddIfHasValue("date", date);
            queryParams.Add("page_size", pageSize);
            queryParams.Add("page_number", pageNumber);

            return GetMultiResultAsync<Fundamental>($"{FundamentalsEndpoint}/reported", queryParams);
        }

        public Task<MultiResult<XBRLTagLabel>> GetAsReportedXBRLTagsAndLabelsAsync(string symbol, Statement statement, DateTime fiscalYear, FiscalPeriod fiscalPeriod, PeriodType? type = null, DateTime? date = null, int? pageSize = null, int? pageNumber = null)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.Add("identifier", symbol);
            queryParams.Add("statement", statement.GetAlternateValue());
            queryParams.Add("fiscal_year", fiscalYear.ToString("yyyy"));
            queryParams.Add("fiscal_period", fiscalPeriod);
            queryParams.AddIfHasValue("type", type);
            queryParams.AddIfHasValue("date", date);
            queryParams.AddIfHasValue("page_size", pageSize);
            queryParams.AddIfHasValue("page_number", pageNumber);

            return GetMultiResultAsync<XBRLTagLabel>($"{TagsEndpoint}/reported", queryParams);
        }

        public Task<MultiResult<XBRLFinancial>> GetAsReportedFinancialsAsync(string symbol, Statement statement, DateTime fiscalYear, FiscalPeriod fiscalPeriod, PeriodType? type = null, DateTime? date = null, int? pageSize = null, int? pageNumber = null)
        {
            var queryParams = new Dictionary<string, object>();

            queryParams.Add("identifier", symbol);
            queryParams.Add("statement", statement.GetAlternateValue());
            queryParams.Add("fiscal_year", fiscalYear.ToString("yyyy"));
            queryParams.Add("fiscal_period", fiscalPeriod);
            queryParams.AddIfHasValue("type", type);
            queryParams.AddIfHasValue("date", date);
            queryParams.AddIfHasValue("page_size", pageSize);
            queryParams.AddIfHasValue("page_number", pageNumber);

            return GetMultiResultAsync<XBRLFinancial>($"{FinancialsEndpoint}/reported", queryParams);
        }
    }
}