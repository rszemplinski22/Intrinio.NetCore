﻿using Intrinio.NetCore.Data.Models.Enums;
using Intrinio.NetCore.Data.Models.Master_Data;
using Intrinio.NetCore.Data.Models.Public_Companies;
using Intrinio.NetCore.Responses;
using System;
using System.Threading.Tasks;

namespace Intrinio.NetCore.Api.Api.Interfaces
{
    public interface IMasterDataFeed
    {

        Task<MultiResult<Company>> GetCompanyMasterAsync(string query = null, DateTime? latestFilingDate = null, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<Security>> GetSecurityMasterAsync(string identifier = null, string query = null, string exchange = null, bool usOnly = false, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<Security>> GetSecurityMasterAsync(string identifier = null, string query = null, StockExchangeEnum? exchange = null, bool usOnly = false, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<Index>> GetIndexMasterAsync(string identifier, IndexType? type = null, Order? order = null, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<Index>> GetIndexMasterByQueryAsync(string query, IndexType? type = null, Order? order = null, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<Owner>> GetOwnerMasterAsync(int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<Owner>> GetOwnerMasterByInstitutionalAsync(bool institutional, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<Owner>> GetOwnerMasterByIndexAsync(string indexKey, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<Owner>> GetOwnerMasterByQueryAsync(string query, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<StockExchange>> GetStockExchangeMasterAsync(string query = null, int pageSize = 250, int pageNumber = 1);

    }
}
