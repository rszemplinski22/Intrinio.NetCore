﻿using Intrinio.NetCore.Data.Models;
using Intrinio.NetCore.Data.Models.Enums;
using Intrinio.NetCore.Data.Models.Usage;
using Intrinio.NetCore.Responses;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Intrinio.NetCore.Api.Interfaces
{
    public interface IUsage
    {

        Task<List<AccessLimits>> GetAccessLimitsAsync();

        Task<CurrentUsage> GetCurrentUsageAsync(string accessCode);

        Task<CurrentUsage> GetCurrentUsageAsync(AccessCode accessCode);

        Task<MultiResult<HistoricalUsage>> GetHistoricalUsageAsync(string accessCode);

        Task<MultiResult<HistoricalUsage>> GetHistoricalUsageAsync(AccessCode accessCode);

    }
}
