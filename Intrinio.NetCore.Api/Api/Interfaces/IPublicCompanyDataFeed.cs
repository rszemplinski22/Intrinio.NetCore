﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Intrinio.NetCore.Data.Models.Enums;
using Intrinio.NetCore.Data.Models.Public_Companies;
using Intrinio.NetCore.Data.Models.Public_Companies.MetaData;
using Intrinio.NetCore.Responses;

namespace Intrinio.NetCore.Api.Api.Interfaces
{
    public interface IPublicCompanyDataFeed
    {
        Task<MultiResult<SECFiling>> GetSECFilingsAsync(string symbol, ReportType? type = ReportType._4, DateTime? startDate = null, DateTime? endDate = null, int? pageSize = 250, int? pageNumber = 1);

        Task<MultiResult<Company>> GetCompaniesAsync(DateTime? latestFilingDate = null, int pageSize = 250, int pageNumber = 1);

        Task<Company> GetCompanyAsync(string symbol);

        Task<DataPoint> GetDataPointAsync(string identifier, string item);
        
        Task<DataPoint> GetDataPointAsync(string identifier, Tag item);

        Task<MultiResult<DataPoint>> GetDataPointFromIdentifiersAsync(string item, params string[] identifiers);

        Task<MultiResult<DataPoint>> GetDataPointFromIdentifiersAsync(Tag item, params string[] identifiers);

        Task<MultiResult<DataPoint>> GetDataPointsAsync(IEnumerable<string> identifiers, IEnumerable<string> items);

        Task<MultiResult<DataPoint>> GetDataPointsFromItemsAsync(string identifier, params string[] items);

        Task<MultiResult<DataPoint>> GetDataPointsFromItemsAsync(string identifier, params Tag[] items);

        Task<MultiResult<HistoricalData>> GetHistoricalDataAsync(string identifier, string item, DateTime? startDate = null, DateTime? endDate = null, 
            Frequency frequency = Frequency.Yearly, PeriodType type = PeriodType.TTM, SortOrder sortOrder = SortOrder.Descending, int? pageSize = null, int? pageNumber = null);

        Task<MultiResult<HistoricalData>> GetHistoricalDataAsync(string identifier, Tag item, DateTime? startDate = null, DateTime? endDate = null,
            Frequency frequency = Frequency.Yearly, PeriodType type = PeriodType.TTM, SortOrder sortOrder = SortOrder.Descending, int? pageSize = null, int? pageNumber = null);

        Task<MultiResult<Price>> GetPricesAsync(string identifier, DateTime? startDate = null, DateTime? endDate = null, Frequency frequency = Frequency.Yearly, 
            SortOrder sortOrder = SortOrder.Descending, int? pageSize = null, int? pageNumber = null);

        Task<MultiResult<CompanyNews>> GetCompanyNewsAsync(string identifier, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<ExchangePrice>> GetExchangePricesAsync(string exchange, DateTime priceDate, int? pageSize = null, int? pageNumber = null);
        
        Task<MultiResult<ExchangePrice>> GetExchangePricesAsync(StockExchangeEnum exchange, DateTime priceDate, int? pageSize = null, int? pageNumber = null);

        Task<MultiResult<Security>> GetAllSecuritiesAsync(int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<Security>> GetSecuritiesWithQueryAsync(string query, DateTime? lastCrspAdjDate = null, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<Security>> GetSecuritiesWithExchangeAsync(string exchangeSymbol, DateTime? lastCrspAdjDate = null, int pageSize = 250, int pageNumber = 1);
        
        Task<MultiResult<Security>> GetSecuritiesWithExchangeAsync(StockExchangeEnum? exchangeSymbol, DateTime? lastCrspAdjDate = null, int pageSize = 250, int pageNumber = 1);

        Task<Security> GetSecurityAsync(string identifier);

        Task<MultiResult<dynamic>> ScreenSecuritiesAsync(Condition condition, SortOrder orderDirection = SortOrder.Ascending, bool primaryOnly = false, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<dynamic>> ScreenSecuritiesAsync(string conditions, string logic, SortOrder orderDirection = SortOrder.Ascending, bool primaryOnly = false, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<Fundamental>> GetStandardizedFundamentalsAsync(string identifier, Statement statement, PeriodType type = PeriodType.FY, DateTime? date = null, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<TagLabel>> GetStandardizedTagsAndLabelsAsync(Statement statement, string ticker = null, Template? template = null, int? pageSize = null, int? pageNumber = null);

        Task<MultiResult<Financial>> GetStandardizedFinancialsAsync(string identifier, Statement statement, DateTime fiscalYear, FiscalPeriod period,
            PeriodType? type = null, DateTime? date = null, int? pageSize = null, int? pageNumber = null);

        Task<MultiResult<Index>> GetAllIndicesAsync(string type = null, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<Index>> GetIndicesByQueryAsync(string query, string type = null, int pageSize = 250, int pageNumber = 1);

        Task<Index> GetIndexAsync(string index);

        Task<Index> GetIndexAsync(StockIndex index);

        Task<MultiResult<Fundamental>> GetAsReportedFundamentalsAsync(string symbol, Statement statement, PeriodType? type = null, DateTime? date = null, int pageSize = 250, int pageNumber = 1);

        Task<MultiResult<XBRLTagLabel>> GetAsReportedXBRLTagsAndLabelsAsync(string symbol, Statement statement, DateTime fiscalYear, FiscalPeriod fiscalPeriod, 
            PeriodType? type = null, DateTime? date = null, int? pageSize = null, int? pageNumber = null);

        Task<MultiResult<XBRLFinancial>> GetAsReportedFinancialsAsync(string symbol, Statement statement, DateTime fiscalYear, FiscalPeriod fiscalPeriod,
            PeriodType? type = null, DateTime? date = null, int? pageSize = null, int? pageNumber = null);
    }
}
