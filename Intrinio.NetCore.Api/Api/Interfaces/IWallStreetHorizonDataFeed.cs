using System.Collections.Generic;
using System.Threading.Tasks;
using Intrinio.NetCore.Data.Models.Enums;
using Intrinio.NetCore.Data.Models.Public_Companies;
using Intrinio.NetCore.Responses;

namespace Intrinio.NetCore.Api.Api.Interfaces
{
    public interface IWallStreetHorizonDataFeed
    {
         Task<DataPoint> GetWallStreetHorizonDataPointAsync(string identifier, string item);
         Task<DataPoint> GetWallStreetHorizonDataPointAsync(string identifier, WallStreetHorizonTag item);
         Task<MultiResult<DataPoint>> GetWallStreetHorizonDataPointsAsync(IEnumerable<string> identifiers, params WallStreetHorizonTag[] items);
         Task<MultiResult<DataPoint>> GetWallStreetHorizonDataPointsAsync(IEnumerable<string> identifiers, IEnumerable<string> items);
    }
}