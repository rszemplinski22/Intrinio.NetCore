using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Intrinio.NetCore.Api.Api.Interfaces;
using Intrinio.NetCore.Data.Extensions;
using Intrinio.NetCore.Data.Models.Enums;
using Intrinio.NetCore.Data.Models.Public_Companies;
using Intrinio.NetCore.Exceptions;
using Intrinio.NetCore.Extensions;
using Intrinio.NetCore.Responses;

namespace Intrinio.NetCore.Api.Api
{
    internal partial class IntrinioApi : IWallStreetHorizonDataFeed
    {
        private const string WallStreetHorizonEndpoint = "data_point";

        public Task<DataPoint> GetWallStreetHorizonDataPointAsync(string identifier, string item)
        {
            var queryParams = new Dictionary<string, object>
            {
                {"identifier", identifier},
                {"item", item}
            };

            return GetAsync<DataPoint>(WallStreetHorizonEndpoint, queryParams);
        }

        public Task<DataPoint> GetWallStreetHorizonDataPointAsync(string identifier, WallStreetHorizonTag item)
        {
            return GetWallStreetHorizonDataPointAsync(identifier, item.GetAlternateValue());
        }

        public Task<MultiResult<DataPoint>> GetWallStreetHorizonDataPointsAsync(IEnumerable<string> identifiers, params WallStreetHorizonTag[] items)
        {
            return GetWallStreetHorizonDataPointsAsync(identifiers, items.Select(x => x.GetAlternateValue()));
        }

        public Task<MultiResult<DataPoint>> GetWallStreetHorizonDataPointsAsync(IEnumerable<string> identifiers, IEnumerable<string> items)
        {
            // Because Intrinio has separate results for when you
            // have multiple vs single I have to add a check for this
            if(identifiers.Count() == 1 && items.Count() == 1)
            {
                throw new IntrinioException("You only have 1 identifier and item. Please use other method call.");
            }

            var queryParams = new Dictionary<string, object>
            {
                {"identifier", identifiers.ToCsv()},
                {"items", items.ToCsv()}
            };

            return GetMultiResultAsync<DataPoint>(WallStreetHorizonEndpoint, queryParams);
        }
    }
}