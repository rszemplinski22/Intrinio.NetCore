using Intrinio.NetCore.Api.Api;
using Intrinio.NetCore.Api.Api.Interfaces;
using Intrinio.NetCore.Api.Interfaces;
using Intrinio.NetCore.Utilities;

namespace Intrinio.NetCore
{
    public class IntrinioClient
    {

        public IUsage Usage => Api;

        public IMasterDataFeed MasterDataFeed => Api;

        public IPublicCompanyDataFeed PublicCompanyDataFeed => Api;

        public IWallStreetHorizonDataFeed WallStreetHorizonDataFeed => Api;

        private IntrinioApi Api { get; }

        public IntrinioClient(string username, string password)
        {
            Api = new IntrinioApi(new Credentials(username, password));
        }

    }
}